import React from 'react';
import {connect} from 'react-redux';
import {
    Container,
    Content,
} from 'native-base';
import {fetchVideos} from "../actions";
import VideoList from '../components/VideoList';
import AppSpinner from '../components/AppSpinner';
import BackHandlerWrapper from '../components/BackHandlerWrapper';
import FaceCamera from '../components/FaceCamera'
import {Text} from "react-native";

class VideosScreen extends React.Component {

    componentWillMount() {
        this.props.fetchVideos(this.props.token);
        console.log("coucou")
    }

    renderContent() {
        return (
            <VideoList
                videos={this.props.videos}
            />
        );
    }

    render() {
        return (
            <Container>
                <Content>
                    {this.renderContent()}
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        token,    
    } = state.user;

    const {
        videos,
        videosLoading,
    } = state.video;

    return {
        token,
        videos,
        videosLoading,
    };
};

export default connect(mapStateToProps, { fetchVideos })(VideosScreen)
