import React from 'react';
import { StyleSheet, Platform, Dimensions } from 'react-native';
import {
    Button,
    Text,
    Container,
    Content,
    H1,
    H3,
    Grid,
    Col,
    Row,
    Icon,
    Card,
    CardItem,
    Body,
    View,
} from 'native-base';
import VideosCarousel from '../components/VideosCarousel';
import { fetchUserStats, fetchVideos } from "../actions";
import { connect } from "react-redux";
import AppSpinner from "../components/AppSpinner";

const styles = StyleSheet.create({
    title: {
        paddingVertical: 24,
    },
    subtitle: {
        marginBottom: 24,
    },
    description: {
        paddingHorizontal: 8,
        marginBottom: 16,
    },
    btn: {
        width: '100%',
        justifyContent: 'flex-start'
    },
    btnRevise: {
        backgroundColor: '#FFD43A'
    },
    btnEasy: {
        backgroundColor: '#20B449'
    },
    btnHard: {
        backgroundColor: '#FF473A',
    },
    btnDemo: {
        backgroundColor: '#0066ff',
    },
    btnProfile: {
        height: 100,
        backgroundColor: '#6DA57F',
        marginTop: 20,
        borderRadius: 10
    },
    btnText: {
        color: '#FFF',
        textShadowColor: 'rgba(0, 0, 0, 0.30)',
        textShadowOffset: { width: 0, height: 3 },
        textShadowRadius: 6,
    },
    labelGray: {
        color: '#979797',
        fontSize: 10
    },
    paddingButton: {
        paddingHorizontal: 20,
        paddingVertical: 0,
        marginVertical: 0,
        maxHeight: 60,
    },
    txtStat: {
        fontSize: 12,
    },
    container: {
    }
});


class HomeScreen extends React.Component {

    componentWillMount() {
        this.props.fetchUserStats(this.props.token);
        this.props.fetchVideos(this.props.token);
    }

    getWordsReadCount() {
        const { wordsRead } = this.props.user;
        let count = 0;
        if (wordsRead.days.length > 0) {
            count = wordsRead.days.map((day) => day.count).reduce((acc, current) => acc + current);
        }
        return count;
    }

    getNewWordsReadCount() {
        const { newWordsRead } = this.props.user;
        let count = 0;
        if (newWordsRead.days.length > 0) {
            count = newWordsRead.days.map((day) => day.count).reduce((acc, current) => acc + current);
        }
        return count;
    }

    render() {
        return (
            <Container padding={20}>
                <Content padder contentContainerStyle={styles.container}>

                    <H1>Start improve your english skills !</H1>

                    <Button
                        full
                        success
                        style={styles.btnProfile}
                        onPress={() => this.props.navigation.navigate('DemoText')}
                    >
                        <H1 style={styles.btnText}>DEMO</H1>
                    </Button>

                    <Button
                        full
                        success
                        style={styles.btnProfile}
                        onPress={() => this.props.navigation.navigate('ChooseLevel')}
                    >
                        <H1 style={styles.btnText}>TEXTS</H1>
                    </Button>

                    <Button
                        full
                        success
                        style={styles.btnProfile}
                        onPress={() => this.props.navigation.navigate('Videos')}
                    >
                        <H1 style={styles.btnText}>VIDEOS</H1>
                    </Button>

                    <Button
                        full
                        success
                        style={styles.btnProfile}
                        onPress={() => this.props.navigation.navigate('ExperimentForm')}
                    >
                        <H1 style={styles.btnText}>EXPERIMENT</H1>
                    </Button>

                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        token,
        user,
        userStatsLoading,
    } = state.user;

    const {
        videos,
        videosLoading,
    } = state.video;

    return {
        token,
        user,
        userStatsLoading,
        videos,
        videosLoading,
    };
};

export default connect(mapStateToProps, { fetchUserStats, fetchVideos })(HomeScreen)
