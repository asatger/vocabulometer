import React from 'react';
import { StyleSheet, ScrollView, TextInput, Platform, Dimensions, Slider } from 'react-native';
import {
    Button,
    Text,
    Container,
    Content,
    H1,
    H3,
    Grid,
    Col,
    Row,
    Icon,
    Card,
    CardItem,
    Body,
    View,
    DatePicker,
    Form,
    Item,
    Picker
} from 'native-base';
import { fetchUserStats, fetchVideos } from "../actions";
import { connect } from "react-redux";
import AppSpinner from "../components/AppSpinner";
import MultiSelect from 'react-native-multiple-select';
import axios from 'axios';

const genders = [
    {
        label: 'Man',
        value: 'man',
    },
    {
        label: 'Female',
        value: 'female',
    },
    {
        label: 'Other',
        value: 'other',
    },
];

const mother_languages = [
    {
        label: 'English',
        value: 'en',
    },
    {
        label: 'French',
        value: 'fr',
    },
    {
        label: 'Japanese',
        value: 'jap',
    },
];

const skills = [
    {
        label: 'Beginner',
        value: '00'
    },
    {
        label: 'Balance',
        value: '03'
    },
    {
        label: 'Fluent',
        value: '04'
    },
];

const interests = [
    {
        id: '1',
        name: 'Entertainment'
    },
    {
        id: '2',
        name: 'Economy'
    },
    {
        id: '3',
        name: 'Environment'
    },
    {
        id: '4',
        name: 'Lifestyle'
    },
    {
        id: '5',
        name: 'Politics'
    },
    {
        id: '6',
        name: 'Sport'
    },
    {
        id: '7',
        name: 'Science'
    },
];

class ExperimentFormScreen extends React.Component {

    constructor(props) {
        super(props)

        this.inputRefs = {
            gender: null,
            mother_language: null,
        };

        this.state = {
            date: "2015-12-31",
            gender: undefined,
            mother_language: undefined,
            skill: 1,
            interests: [],
            id_experiment: 0,
        };

        this.id_experiment = 0;
    }

    submitForm = () => {
        return new Promise((resolve, reject) => {

            let interests = this.state.interests;

            let id_lang = 0;

            let url = "https://vocabulometer.iutlppro.devops.u-bordeaux.fr/api/v1/";
            //let url = "http://localhost:8080/api/v1/";

            /**
             * Récupération de l'id de la langue
             */
            axios.get(url + `vocabulospeeds/lang/` + this.state.mother_language)
                .then(res => {
                    id_lang = res.data[0].id;

                    /**
                     *  Ajouter une expérience
                     */
                    axios.post(url + 'experiment/', {
                        age: 0,
                        gender: this.state.gender,
                        id_lang: id_lang,
                        english_skill: this.state.skill
                    })
                        .then(function (response) {
                            this.id_experiment = response.data;

                            for (let i = 0; i < interests.length; i++) {
                                axios.post(url + 'experiments/incentive', {
                                    id_experiment: this.id_experiment,
                                    id_incentive: interests[i]
                                })
                                    .then(function (response) {
                                        resolve(this.id_experiment);
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                });
        });
    }

    render() {
        return (

            <ScrollView style={styles.container}>

                <Form>

                    <Text>Gender:</Text>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            placeholder="Select a gender..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.gender}
                            onValueChange={value => this.setState({ gender: value })}
                        >
                            {genders.map((gender, index) => {
                                return (<Picker.Item label={gender.label} value={gender.value} key={index} />)
                            })}
                        </Picker>
                    </Item>

                    <View paddingVertical={10} />

                    <Text>Mother language:</Text>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            placeholder="Select a mother language..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.mother_language}
                            onValueChange={value => this.setState({ mother_language: value })}
                        >
                            {mother_languages.map((lang, index) => {
                                return (<Picker.Item label={lang.label} value={lang.value} key={index} />)
                            })}
                        </Picker>
                    </Item>

                    <View paddingVertical={10} />

                    <Text>English skill:</Text>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            placeholder="Select a mother language..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.skill}
                            onValueChange={value => this.setState({ skill: value })}
                        >
                            {skills.map((gender, index) => {
                                return (<Picker.Item label={gender.label} value={gender.value} key={index} />)
                            })}
                        </Picker>
                    </Item>

                    <View paddingVertical={10} />

                    <Text>Your interests:</Text>

                    <View paddingVertical={10} />
                    
                    <MultiSelect
                        hideTags
                        hideSubmitButton
                        autoFocusInput={false}
                        items={interests}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={items => this.setState({ interests: items })}
                        selectedItems={this.state.interests}
                        selectText="Select interests..."
                        searchInputPlaceholderText="Search Items..."
                        onChangeInput={(text) => console.log(text)}
                        selectedItemTextColor="#6DA57F"
                        selectedItemIconColor="#6DA57F"
                        displayKey="name"
                    />

                    <View paddingVertical={10} />

                    <Button
                        full
                        style={styles.btn}
                        onPress={() => {
                            this.submitForm().then((res) => {
                                this.props.navigation.navigate('TextExperiment', {
                                    id_experiment: res
                                });
                            });

                        }}
                    >
                        <Text>SUBMIT</Text>
                    </Button>
                </Form>

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 40,
        paddingHorizontal: 40,
        flex: 1,
    },

    btn: {
        marginTop: 16,
        marginHorizontal: 40,
        backgroundColor: '#6DA57F',
        borderRadius: 10
    }
});

const mapStateToProps = (state) => {
    const {
        token,
        user,
    } = state.user;

    return {
        token,
        user,
    };
};

export default connect(mapStateToProps, { fetchUserStats, fetchVideos })(ExperimentFormScreen)
