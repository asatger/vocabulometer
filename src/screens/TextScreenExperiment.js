import React, { Component } from 'react';
import { fetchSingleText } from '../actions';
import { connect } from 'react-redux';
import { View, Text, Button, ScrollView } from 'react-native';
import { Icon } from 'native-base';
import Circle from 'react-native-progress/Circle';
import platform from "../../native-base-theme/variables/platform";
import PageSwiperExperiment from '../components/PageSwiperExperiment';
import axios from "axios";

    const styles = {
    spinnerContainer: {
        justifyContent: 'center',
        flex: 1,
    },
    paragraph: {
        marginVertical: 8,
    },
    title: {
        fontSize: 24,
        textAlign: 'center'
    },
    subTitle: {
        fontSize: 18,
        textAlign: 'center',
        marginBottom: 10
    },
    container: {
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 30
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    box: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        backgroundColor: "#6FB98F",
        margin: 30
    },
    badgeGreen: {
        borderRadius: 4,
        backgroundColor: "#6FB98F",
        padding: 5
    },
    badgeRed: {
        borderRadius: 4,
        backgroundColor: "#e74c3c",
        padding: 5
    },
    card: {
        flex: 1,
        backgroundColor: "#ffffff",
        margin: 10,
        borderRadius: 10,
    },
    paragraphTitle: {
        fontSize: 20,
        textAlign: "center",
        fontWeight: "bold",
        margin: 10,
    },
    rowContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    red: {
        backgroundColor: "#e55039"
    },
    green: {
        backgroundColor: "#2ed573"
    }
};

class TextScreen extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        textLength: 0,
        NumberOfParagraphs: 0,
        textFinished: false,
        switchs: [],
        switchsValue: [],
        text: "",
        text_id: 0
    };

    componentDidMount() {
        this.getText().then((r) => {
            this.setState({
               text: r
            });
        })
    }

    finish = (switchsValue) => {
        this.setState({
            textFinished: true,
            switchsValue: switchsValue
        })
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    getText() {
        return new Promise((resolve, reject) => {

            let random = Math.round(Math.random()*7+7);

             this.setState({
                 text_id: random
             }, () => {
                 axios.get(`https://vocabulometer.iutlppro.devops.u-bordeaux.fr/api/v1/text/` + this.state.text_id)
                     .then(res => {
                         resolve(res.data.text);
                     })
                     .catch(function (error) {
                         console.log(error);
                     });
             });
        });

    }

    render() {
        if (this.props.singleTextLoading) {
            return (
                <View style={styles.spinnerContainer}>
                    <Circle size={30} indeterminate={true} color={platform.brandPrimary} style={{ alignSelf: 'center' }} />
                </View>
            )
        }

        return (
            !this.state.textFinished ?
                <PageSwiperExperiment
                    navigation={this.props.navigation}
                    data={this.state.text}
                    finish={this.finish}
                    predictReading={this.predictReading}
                    textid={this.state.text_id}
                    experimentid={this.props.navigation.getParam('id_experiment', '1')}
                >
                </PageSwiperExperiment>
                :
                <ScrollView>
                    <Text>
                        Merci pour le test !
                    </Text>
                    <Button
                        onPress={this.goBack}
                        title="Retourner à l'index"
                        color="#6FB98F">
                    </Button>
                </ScrollView>
        );
    }
}

const mapStateToProps = ({ text, user }) => {
    const {
        singleText,
        singleTextLoading,
    } = text;
    const { token } = user;

    return {
        singleText,
        singleTextLoading,
        token,
    };
};

const mapDispatchToProps = {
    fetchSingleText,
};

export default connect(mapStateToProps, mapDispatchToProps)(TextScreen);