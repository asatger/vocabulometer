import React from 'react';
import { StyleSheet, Platform, Dimensions } from 'react-native';
import {
    Button,
    Text,
    Container,
    Content,
    H1,
    H3,
    Grid,
    Col,
    Row,
    Icon,
    Card,
    CardItem,
    Body,
    View,
} from 'native-base';
import VideosCarousel from '../components/VideosCarousel';
import { fetchUserStats, fetchVideos } from "../actions";
import { connect } from "react-redux";
import AppSpinner from "../components/AppSpinner";

const styles = StyleSheet.create({
    title: {
        paddingVertical: 24,
    },
    subtitle: {
        marginBottom: 24,
    },
    description: {
        paddingHorizontal: 8,
        marginBottom: 16,
    },
    btn: {
        marginTop: 20,
        borderRadius: 10,
        height: 100,
        justifyContent: 'center'
    },
    btnRevise: {
        backgroundColor: '#FFD43A'
    },
    btnEasy: {
        backgroundColor: '#20B449'
    },
    btnHard: {
        backgroundColor: '#FF473A',
    },
    btnProfile: {
        backgroundColor: '#6DA57F',
    },
    btnText: {
        color: '#FFF',
        textShadowColor: 'rgba(0, 0, 0, 0.30)',
        textShadowOffset: {width: 0, height: 3},
        textShadowRadius: 6,
    },
    labelGray: {
        color: '#979797',
        fontSize: 10
    },
    paddingButton: {
        paddingHorizontal: 20,
        paddingVertical: 0,
        marginVertical: 0,
        maxHeight: 60,
    },
    txtStat: {
        fontSize: 12,
    },
    container: {
    }
});


class ChooseLevelScreen extends React.Component {

    componentWillMount() {
        this.props.fetchUserStats(this.props.token);
        this.props.fetchVideos(this.props.token);
    }

    getWordsReadCount() {
        const { wordsRead } = this.props.user;
        let count = 0;
        if (wordsRead.days.length > 0) {
            count = wordsRead.days.map((day) => day.count).reduce((acc, current) => acc + current);
        }
        return count;
    }

    getNewWordsReadCount() {
        const { newWordsRead } = this.props.user;
        let count = 0;
        if (newWordsRead.days.length > 0) {
            count = newWordsRead.days.map((day) => day.count).reduce((acc, current) => acc + current);
        }
        return count;
    }

    render() {
        if (this.props.userStatsLoading || this.props.videosLoading) {
            return (
                <Container style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <AppSpinner />
                </Container>
            );
        }

        return (
            <Container padding={20}>
                <Content padder contentContainerStyle={styles.container}>

                    <H1>Choose your level</H1>

                    <Button
                        full
                        success
                        style={[styles.btn, styles.btnEasy]}
                        onPress={() => this.props.navigation.navigate('EasyTexts')}
                    >
                        <Icon
                            type="FontAwesome"
                            name='thermometer-empty'
                        />
                        <H1 style={styles.btnText}>EASY</H1>
                    </Button>

                    <Button
                        full
                        success
                        style={[styles.btn, styles.btnRevise]}
                        onPress={() => this.props.navigation.navigate('RecommendedTexts')}
                    >
                        <Icon
                            type="FontAwesome"
                            name='thermometer-half'
                        />
                        <H1 style={styles.btnText}>BALANCE</H1>
                    </Button>

                    <Button
                        full
                        success
                        style={[styles.btn, styles.btnHard]}
                        onPress={() => this.props.navigation.navigate('HardTexts')}
                    >
                        <Icon
                            type="FontAwesome"
                            name='thermometer-full'
                        />
                        <H1 style={styles.btnText}>HARD</H1>
                    </Button>

                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const {
        token,
        user,
        userStatsLoading,
    } = state.user;

    const {
        videos,
        videosLoading,
    } = state.video;

    return {
        token,
        user,
        userStatsLoading,
        videos,
        videosLoading,
    };
};

export default connect(mapStateToProps, { fetchUserStats, fetchVideos })(ChooseLevelScreen)
