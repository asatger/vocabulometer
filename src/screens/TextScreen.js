import React, { Component } from 'react';
import { fetchSingleText } from '../actions';
import { connect } from 'react-redux';
import { View, Text, Button, ScrollView } from 'react-native';
import { Container, Icon } from 'native-base';
import Circle from 'react-native-progress/Circle';
import platform from "../../native-base-theme/variables/platform";
import PageSwiper from '../components/PageSwiper';
import BackHandlerWrapper from '../components/BackHandlerWrapper';
import FaceCamera from "../components/FaceCamera";
import SwitchParagraphRead from '../components/SwitchParagraphRead';
import autoMergeLevel1 from 'redux-persist/es/stateReconciler/autoMergeLevel1';

const BASE_URL = "https://vocabulometer.iutlppro.devops.u-bordeaux.fr/api/v1";

const styles = {
    spinnerContainer: {
        justifyContent: 'center',
        flex: 1,
    },
    title: {
        fontSize: 24,
        textAlign: 'center'
    },
    subTitle: {
        fontSize: 18,
        textAlign: 'center',
        marginBottom: 10
    },
    container: {
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 30
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    card: {
        flex: 1,
        backgroundColor: "#ffffff",
        margin: 10,
        borderRadius: 10,
    },
    paragraphTitle: {
        fontSize: 20,
        textAlign: "center",
        fontWeight: "bold",
        margin: 10,
    },
    rowContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    red: {
        backgroundColor: "#e55039"
    },
    green: {
        backgroundColor: "#2ed573"
    }
};

class TextScreen extends Component {

    state = {
        NumberOfParagraphs: 0,
        textFinished: false,
        switchs: [],
        wordsPerMinuteArray: [],
        paragraphsRead: [],
        paragraphsNotRead: [],
        switchsValue: [],
        watchingPercentages: []
    };

    componentWillMount() {
        const text = this.props.navigation.state.params;
        console.log("text : " + text);
        text === undefined ? this.props.fetchSingleText(this.props.token, "5b332e7d5c4e4f50b893f0ed")
            : this.props.fetchSingleText(this.props.token, text.uri);
    }

    buildParagraph(item) {
        let string = '';
        const { interWords, words } = item;
        for (let i = 0; i < item.words.length; i++) {
            string += interWords[i];
            string += words[i].raw;
        }
        string += interWords[interWords.length - 1];

        return string;
    }

    buildText() {
        let paragraphs = [];
        const maxParagraphLength = 750;
        for (const item of this.props.singleText.body) {
            const paragraph = this.buildParagraph(item);

            if (paragraph.length > maxParagraphLength) {
                let splittedParagraph = paragraph;
                let charactersRemaining = paragraph.length;
                while (charactersRemaining > maxParagraphLength) {

                    if (splittedParagraph[maxParagraphLength] !== ' ' || splittedParagraph[maxParagraphLength] !== '.') {
                        let isNextCharacterBlankOrDot = false;
                        let i = maxParagraphLength + 1;
                        while (!isNextCharacterBlankOrDot) {
                            if (splittedParagraph[i] === ' ' || splittedParagraph[i] === '.') {
                                paragraphs.push(splittedParagraph.substr(0, i + 1));
                                splittedParagraph = splittedParagraph.slice(i + 1);
                                charactersRemaining -= i + 1;
                                isNextCharacterBlankOrDot = true;
                                break;
                            }
                            i++;
                        }
                    } else {
                        paragraphs.push(splittedParagraph.substr(0, 700));
                        splittedParagraph = splittedParagraph.slice(700);
                        charactersRemaining -= 700;
                    }
                }
                paragraphs.push(splittedParagraph);
            } else {
                paragraphs.push(paragraph);
            }
        }
        this.state.NumberOfParagraphs = paragraphs.length;
        return paragraphs;
    }

    finish = (data) => {
        this.setState({
            textFinished: true,
            switchsValue: data.switchsValue
        })

        let NumberOfParagraphs = this.state.NumberOfParagraphs;
        let paragraphsRead = this.state.paragraphsRead;

        //adding a reading
        fetch(`${BASE_URL}/reading`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id_user: 1
            })
        })
            .then((res) => {
                const { status } = res;
                console.log(status);
                res.json()
                    .then(function (dataResponse) {
                        console.log('Request succeeded with JSON response', dataResponse);
                        if (status === 200) {
                            for (let i = 0; i < NumberOfParagraphs; i++) {
                                let prediction;

                                paragraphsRead.indexOf(i) != -1 ? prediction = true : prediction = false;

                                console.log(data.timeReading[i]);

                                //adding a paragraph
                                fetch(`${BASE_URL}/paragraph`, {
                                    method: 'POST',
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify({
                                        content: data.paragraphs[i],
                                        words_number: data.paragraphs[i].length,
                                        time_spent: data.timeReading[i],
                                        words_per_minute: data.wordsPerMinuteArray[i],
                                        authenticity: data.watchingPercentage[i],
                                        id_reading: dataResponse,
                                        read_prediction: prediction,
                                        read_user_declaration: data.switchsValue[i]
                                    })

                                })
                                    .then(function (dataResponse) {
                                        console.log('Request succeeded with JSON response', dataResponse);
                                    })
                                    .catch(function (error) {
                                        console.log('Request failed', error);
                                    });
                            }
                        }
                    })
                    .catch(function (error) {
                        console.log('Request failed', error);
                    })
            })
            .catch(function (error) {
                console.log('Request failed', error);
            })

    }

    //algorithm of reading prediction
    predictReading = (wordsPerMinuteArray, watchingPercentages) => {
        this.state.wordsPerMinuteArray = wordsPerMinuteArray;
        this.state.watchingPercentages = watchingPercentages;
        let cpt = 0; 
        let cumul = 0;
        let average;
        let read = [];
        let notRead = [];

        //first pass
        for (let i = 0; i < wordsPerMinuteArray.length; i++) {

            //excluding impossible speeds 
            if (wordsPerMinuteArray[i] >= 40 && wordsPerMinuteArray[i] <= 700) {
                cumul += wordsPerMinuteArray[i];
                read.push(wordsPerMinuteArray[i]);
                cpt++;
            } else {
                this.state.paragraphsNotRead.push(i);
                notRead.push(wordsPerMinuteArray[i]);
            }
        }
        average = cumul / cpt; //average of words per minute
        console.log("average : " + average)

        //second path with outliers exluded
        for (let i = 0; i < read.length; i++) {
            let gap = (read[i] / average * 100); //gap between the word read per minute and the average 
            let index = wordsPerMinuteArray.indexOf(read[i]); //paragraph index
            console.log("gap : " + gap);

            //excluding average outliers
            if (gap < 50 || gap > 120) {
                console.log("gap too high, paragraph " + (index + 1) + " was not read");
                this.state.paragraphsNotRead.push(index);
                notRead.push(read[i]);
            } else {
                console.log("percentage : " + watchingPercentages[index])
                console.log("gap in limits, paragraph " + (index + 1) + " was read");

                //keeping only watched paragraph
                if (watchingPercentages[index] >= 70) {
                    console.log("percentage watching the screen good, paragraph " + (index + 1) + " was read");
                    this.state.paragraphsRead.push(index);
                } else {
                    console.log("percentage watching the screen not good, paragraph " + (index + 1) + " was not read");
                    this.state.paragraphsNotRead.push(index);
                }
            }
        }
        console.log("read : " + read);
        console.log("not read : " + notRead);
        console.log("read : " + this.state.paragraphsRead);
        console.log("not read : " + this.state.paragraphsNotRead);
        return this.state.paragraphsRead;
    }

    //creating the last page, showing predictions compared to user declaration
    creatingPredictionDisplay = () => {
        let paragraphs = [];

        for (let i = 0; i < this.state.wordsPerMinuteArray.length; i++) {
            paragraphs.push(
                <View style={styles.row}>
                    {
                        <View style={{
                            ...styles.card
                        }}>
                            <View style={{
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                                ...this.state.switchsValue[i] ?
                                    this.state.paragraphsRead.indexOf(i) != -1 ?
                                        styles.green
                                        :
                                        styles.red
                                    :
                                    this.state.paragraphsNotRead.indexOf(i) != -1 ?
                                        styles.green
                                        :
                                        styles.red
                            }}>
                                <Text style={styles.paragraphTitle}>
                                    Paragraph {i + 1}
                                </Text>
                            </View>
                            <View style={styles.rowContainer}>
                                <View style={{ alignItems: "center" }}>
                                    <Text style={styles.subTitle}>What we predicted</Text>
                                    {
                                        this.state.paragraphsRead.indexOf(i) == -1 ?
                                            <Icon
                                                name="times-circle"
                                                type="FontAwesome">
                                            </Icon>
                                            :
                                            <Icon
                                                name="check-circle"
                                                type="FontAwesome">
                                            </Icon>
                                    }
                                </View>
                                <View style={{ alignItems: "center" }}>
                                    <Text style={styles.subTitle}>What you declared</Text>
                                    {
                                        !this.state.switchsValue[i] ?
                                            <Icon
                                                name="times-circle"
                                                type="FontAwesome">
                                            </Icon>
                                            :
                                            <Icon
                                                name="check-circle"
                                                type="FontAwesome">
                                            </Icon>
                                    }
                                </View>
                            </View>
                        </View>
                    }
                </View>
            )
        }
        return paragraphs;
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        if (this.props.singleTextLoading) {
            return (
                <View style={styles.spinnerContainer}>
                    <Circle size={30} indeterminate={true} color={platform.brandPrimary} style={{ alignSelf: 'center' }} />
                </View>
            )
        }

        const paragraphs = this.buildText();
        return (
            !this.state.textFinished ?
                <PageSwiper
                    initialText={this.props.singleText}
                    navigation={this.props.navigation}
                    data={paragraphs}
                    //textTitle={this.props.navigation.state.params.title}
                    finish={this.finish}
                    predictReading={this.predictReading}
                >
                </PageSwiper>
                :
                <ScrollView>
                    <Text style={styles.title}>
                        Did you read ?
                    </Text>
                    <View>
                        <View style={styles.container}>
                            {this.creatingPredictionDisplay()}
                        </View>
                        <View style={{
                            margin: 20,
                        }}>
                            <Button
                                onPress={this.goBack}
                                title="Ok"
                                color="#6FB98F">
                            </Button>
                        </View>
                    </View>
                </ScrollView>
        );

    }
}

const mapStateToProps = ({ text, user }) => {
    const {
        singleText,
        singleTextLoading,
    } = text;
    const { token } = user;

    return {
        singleText,
        singleTextLoading,
        token,
    };
};

const mapDispatchToProps = {
    fetchSingleText,
};

export default connect(mapStateToProps, mapDispatchToProps)(TextScreen);