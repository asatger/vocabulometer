import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, TextInput, Button } from 'react-native';
import {
    View,
    Text,
    StyleProvider,
    Switch,
    Row,
    Icon,
} from 'native-base';
import Swiper from 'react-native-deck-swiper';
import { connect } from 'react-redux';
import { sendWordsRead } from '../actions';
import BackHandler from '../components/BackHandlerWrapper';
import FaceCamera from "./FaceCamera";
import SwitchParagraphRead from '../components/SwitchParagraphRead';


import axios from 'axios';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: -30,
    },
    card: {
        padding: 16,
        flex: 1,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: "#E8E8E8",
        backgroundColor: "white",
        marginBottom: 35
    },
    text: {
        paddingVertical: 8,
        textAlign: 'left',
        fontSize: 15,
        backgroundColor: "transparent"
    },
    pageNumber: {
        alignSelf: 'flex-end',
    },
    centeredContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredTitle: {
        textAlign: "center",
        fontSize: 24
    },
    centeredSubTitle: {
        textAlign: "center",
        fontSize: 18
    },
    SwitchBox: {
        marginBottom: 20,
        backgroundColor: "#f1f2f6",
        padding: 10,
        borderRadius: 10
    },
    switch: {
        flex: 1,
        alignItems: "flex-end",
        marginRight: 10
    },
    switchContainer: {
        flexDirection: "row",
    },
    textBox: {
        marginTop: 5,
        marginRight: 20,
        marginLeft: 20,
        justifyContent: "center",
    }
});

class PageSwiper extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        cardIndex: 0,
        paragraphReadTime: [],
        wordsPerMinute: 0,
        wordsPerMinuteArray: [],
        timeCalculated: false,
        faceDetected: false,
        intervalWatching: [0, 0], //0 : starting to watch (detect face) ; 1 : stop watching (detect face)
        timeWatching: [],
        timeReading: [],
        watchingPercentage: [],
        cameraLoaded: false,
        switchsValue: [],
        paragraphs: []
    }

    sendWordsRead() {
        words = this.props.initialText.body.map(paragraph => {
            return paragraph.words.map(object => object.lemma).filter(word => word !== undefined)
        });
        console.log("words : ")
        console.log(words);
        
        const finalWordsArray = [];
        for (const wordArray of words) {
            finalWordsArray.push(...wordArray);
        }
        let arr = [];
        //console.log(finalWordsArray);
        arr.push(...finalWordsArray.filter((word, pos, self) => self.indexOf(word) == pos));
        // console.log(arr);
        this.props.sendWordsRead(this.props.token, arr);
    }


    //giving a level of reading to the user, based on averages
    //!! not used in this version !!
    getLevel() {
        var level = null;
        var wpm = this.state.wordsPerMinute; //creating a local copy in order to use it easily
        //console.log("words per minute : " + wpm);
        var start = 0;
        for (let end = start + 100; end <= 700; end += 100) {
            if (this.state.wordsPerMinute > start && this.state.wordsPerMinute <= end) {
                switch (start) {
                    case 0:
                        level = "very slow";
                        break;
                    case 100:
                        level = "slow";
                        break;
                    case 200:
                    case 200:
                        level = "average";
                        break;
                    case 300:
                        level = "good";
                        break;
                    case 400:
                        level = "very good";
                        break;
                    case 500:
                        level = "very fast";
                        break;
                    case 600:
                        level = "awesome";
                        break;
                    default:
                        level = null;
                        break;
                }
            }
            start += 100;
        }
        //console.log(level);
        return level;
    }

    //when a card is switched
    onSwiped = (cardIndex, direction) => {

        //stopping facing detection of the passed card
        this.state.faceDetected ? this.state.intervalWatching[1] = Date.now() : null; 
        //calculate the time watching the screen of the passed card
        this.state.faceDetected ? this.state.timeWatching[cardIndex] += this.state.intervalWatching[1] - this.state.intervalWatching[0] : null; 
        //time when the user start to read the new paragraph
        this.state.paragraphReadTime.push(Date.now()); 
        //time reading the paragraph
        this.state.timeReading[cardIndex] += (this.state.paragraphReadTime[this.state.paragraphReadTime.length - 1] - this.state.paragraphReadTime[this.state.paragraphReadTime.length - 2]); 

        console.log("time reading on paragraph " + (cardIndex + 1) + " : " + this.state.timeReading[cardIndex] / 1000);
      

        //if it is the last paragraph and the time has not been already calculated
        if (cardIndex + 1 == this.props.data.length && !this.state.timeCalculated) {
            for (i = 0; i < this.props.data.length; i++) {
                if (i < this.props.data.length) {
                    console.log("percentage of time watching the screen on the paragraph " +
                        (i + 1) + " : " +
                        (this.state.timeWatching[i] / this.state.timeReading[i]) * 100 + "%");
                    this.state.watchingPercentage[i] = Math.round(this.state.timeWatching[i] / this.state.timeReading[i] * 100);

                    //cardIndex > 0 ? time = time - 1000 : null; // removing a second which is the estimated time between two cards

                    console.log("-----------------------");
                    console.log("Time spent on page " + (i + 1) + " : ");
                    console.log(this.state.timeReading[i] / 1000 + " for " + this.props.data[i].split(' ').length + " words");
                    console.log("-----------------------");
                    this.state.wordsPerMinuteArray.push(this.props.data[i].split(' ').length / this.state.timeReading[i] * 1000 * 60);
                    this.state.wordsPerMinute += (this.props.data[i].split(' ').length / this.state.timeReading[i] * 1000 * 60);
                    console.log("words per minutes on paragraph " + (i + 1) + " " + (this.props.data[i].split(' ').length / this.state.timeReading[i] * 1000 * 60));
                    console.log("-----------------------");
                }
            }
            this.state.timeCalculated = true; //saying that we already calculated the reading time of the text
            this.state.wordsPerMinute = this.state.wordsPerMinute / this.props.data.length; // calculating the words per minute ratio of the whole text
        }
        this.setState({
            cardIndex: direction === 'left' ? cardIndex + 1 : cardIndex - 1
        })

        //resetting intervals to calculate watching time of the next paragraph
        this.state.faceDetected ? this.state.intervalWatching[0] = Date.now() : this.state.intervalWatching[0] = 0;
        this.state.intervalWatching[1] = 0;
    };

    getDetectedFace = (isDetected) => { //Do the user watch the screen or not
        if (!isDetected) //not watching
        {
            if (this.state.faceDetected) { //if he was watching right before
                //this.state.timeWatching[this.state.cardIndex] >= 0 ? null : this.state.timeWatching[this.state.cardIndex] = 0 ;
                this.state.intervalWatching[1] = Date.now(); //stop the interval
                this.state.timeWatching[this.state.cardIndex] += this.state.intervalWatching[1] - this.state.intervalWatching[0]; //calculating time watching
                console.log("not watching, time watching the screen : " + this.state.timeWatching[this.state.cardIndex] / 1000 + " seconds");
            }
        } else //watching
        {
            if (!this.state.cameraLoaded) {
                console.log("camera loaded");
                this.setState({
                    cameraLoaded: true,
                    cardIndex: 0,
                    intervalWatching: [Date.now(), 0],
                    paragraphReadTime: [Date.now()]
                });
            }
            !this.state.faceDetected ? this.state.intervalWatching[0] = Date.now() : null; //starting the interval
            !this.state.faceDetected ? console.log("watching") : null;
        }
        this.state.faceDetected = isDetected; //updating the status
    }

    toggleSwitch = (switchID, switchValue) => {
        this.state.switchsValue[switchID] = switchValue;
        return switchValue;
    }


    render() {
        const finalData = Array.from(this.props.data);
        finalData.push('Congrats !');

        this.state.cardIndex < this.props.data.length ? this.state.paragraphs[this.state.cardIndex] = this.props.data[this.state.cardIndex]: null;

        console.log(this.state.cardIndex);
        //setting watching and reading time of the current paragraph to 0 (for the beggining)  
        this.state.timeWatching[this.state.cardIndex] == null ? this.state.timeWatching[this.state.cardIndex] = 0 : null;
        this.state.timeReading[this.state.cardIndex] == null ? this.state.timeReading[this.state.cardIndex] = 0 : null;

        //setting switch value to true for the current paragraph
        this.state.switchsValue[this.state.cardIndex] == null ? 
        this.state.cardIndex < this.props.data.length ? this.state.switchsValue[this.state.cardIndex] = true : null
        : null;

        return (
            <View style={styles.container}>
                <BackHandler />
                <FaceCamera
                    getDetectedFace={this.getDetectedFace}
                >
                </FaceCamera>
                {
                    !this.state.cameraLoaded ?
                        <View>
                            <Text style={{
                                marginBottom: "100%",
                                textAlign: "center",
                                fontSize: 24
                            }}>
                                Loading the camera {`\n`}
                                <Text style={styles.centeredSubTitle}>
                                    Please position the phone so that it recognizes your face in order to start
                                </Text>
                            </Text>
                        </View>
                        :
                        <Swiper
                            goBackToPreviousCardOnSwipeRight
                            disableRightSwipe={this.state.cardIndex === 0}
                            cards={finalData}
                            onSwipedLeft={(cardIndex) => this.onSwiped(cardIndex, 'left')}
                            onSwipedRight={(cardIndex) => this.onSwiped(cardIndex, 'right')}
                            showSecondCard={false}
                            cardVerticalMargin={50}
                            renderCard={(card, index) => {
                                return (
                                    <ScrollView contentContainerStyle={styles.card}>
                                        {
                                            index < this.props.data.length ?
                                                <View style={styles.SwitchBox}>
                                                    <View style={styles.switchContainer}>
                                                        <Text style={styles.textBox}>
                                                            Did you read the text ?
                                                        </Text>
                                                        <View style={styles.switch}>
                                                            <SwitchParagraphRead
                                                                toggleSwitch={this.toggleSwitch}
                                                                switchID={index}
                                                                switchValue={this.state.switchsValue[this.state.cardIndex]}>
                                                            </SwitchParagraphRead>
                                                        </View>
                                                    </View>
                                                </View> : null
                                        }
                                        {
                                            index == this.props.data.length ?
                                                <View style={styles.centeredContainer}>
                                                    <Text style={styles.centeredTitle}>Congrats !</Text>
                                                    <Image
                                                        style={{ width: 200, height: 200 }}
                                                        source={require('../../assets/trophy.png')}
                                                    />
                                                    <Text style={styles.centeredSubTitle}> Please swipe to the left to see if we have found which paragraphs you have not read</Text>
                                                </View>
                                                :
                                                <Text style={styles.text}>{card}</Text>
                                        }
                                        {
                                            index < this.props.data.length
                                                ? <Text style={styles.pageNumber}>
                                                    {`Page ${index + 1} / ${this.props.data.length}`}
                                                </Text>
                                                : null
                                        }
                                    </ScrollView>
                                )
                            }}
                            onSwipedAll={() => {
                                var paragraphsRead = this.props.predictReading(this.state.wordsPerMinuteArray, this.state.watchingPercentage);
                                console.log("paragraphsRead : " + paragraphsRead);
                                this.props.finish(this.state);
                                this.sendWordsRead(paragraphsRead);
                            }}
                            backgroundColor={'transparent'}
                            stackSize={this.props.data.length}>

                        </Swiper>

                }
            </View>
        );
    }
}

const mapStateToProps = ({ user }) => {
    const { token } = user;
    return { token };
};

export default connect(mapStateToProps, { sendWordsRead })(PageSwiper);
