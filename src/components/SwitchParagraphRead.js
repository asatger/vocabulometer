import React, { Component } from 'react'
import { View, Switch, StyleSheet, Text }

    from 'react-native'

export default class SwitchParagraphRead extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        switchID: this.props.switchID,
        switchValue: this.props.switchValue
    }

    toggleSwitch = (value) => {
        this.setState({
            switchValue: this.props.toggleSwitch(this.state.switchID, value)
        });
    }

    render() {
        return (
            <Switch
                onValueChange={this.toggleSwitch}
                value={this.state.switchValue}
            >
            </Switch>
        )
    }
}
