import React from 'react';
import {
    Text,
    List,
    ListItem,
    H1,
    Button,
    Icon
} from 'native-base';
import { Image, View, Dimensions } from 'react-native';
import { withNavigation } from 'react-navigation';
const { width } = Dimensions.get('window');
import commonColor from '../../native-base-theme/variables/commonColor';

class VideoList extends React.Component {

    renderListItems() {
        return (
            this.props.videos.map((video, i) => {
                return (
                    <ListItem noIndent button key={i}
                        onPress={() => this.props.navigation.navigate('Video', video)}
                    >
                        <View>
                            <Image
                                source={{ uri: `https://img.youtube.com/vi/${video.youtubeId}/0.jpg` }}
                                style={{
                                    width: width - 35,
                                    height: 200,
                                }}
                            />
                            <Text
                                style={{
                                    paddingTop: 8,
                                    paddingLeft: 8,
                                    fontSize: 14,
                                    fontWeight: 'bold',
                                    color: 'white',
                                    position: 'absolute',
                                }}
                            >
                                {video.title}

                            </Text>
                            <Button
                                full
                                style={{
                                    backgroundColor: '#6DA57F'
                                }}
                                onPress={() => this.props.navigation.navigate('Video', video)}
                            >
                                <Text>WATCH IT</Text>
                                <Icon name="play" type="Ionicons" />
                            </Button>
                        </View>
                    </ListItem>
                );
            })
        );
    }

    render() {
        return (
            <List noIndent>
                {this.renderListItems()}
            </List>
        );
    }
}


export default withNavigation(VideoList);
