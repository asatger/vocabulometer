import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Permissions, Camera, FaceDetector, DangerZone } from 'expo'

export default class FaceCamera extends React.Component {

    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    static defaultProps = {
        countDownSeconds: 5,
        motionInterval: 500, //ms between each device motion reading
        motionTolerance: 1, //allowed variance in acceleration
        cameraType: Camera.Constants.Type.front, //front vs rear facing camera
    }
    state = {
        hasCameraPermission: null,
        faceDetecting: false,
        faceDetected: false, //when true, we've found a face
        countDownSeconds: 5, //current available seconds before photo is taken
        countDownStarted: false, //starts when face detected
        pictureTaken: false, //true when photo has been taken
        motion: null, //captures the device motion object
        detectMotion: false, //when true we attempt to determine if device is still
        message: "No face detected",
        cameraLoaded: false,
    };

    static defaultProps = {
        motionInterval: 200,
        motionTolerance: 1,
        cameraType: Camera.Constants.Type.front,
    }

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);

        if(this._isMounted) {
            this.setState({ hasCameraPermission: status === 'granted' });
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    componentDidMount() {
        this.motionListener = DangerZone.DeviceMotion.addListener(this.onDeviceMotion);
        this.detectMotion(true);
        this.setState({message: "Face detected"});
        this._isMounted = true;
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state.detectMotion && nextState.motion && this.state.motion && this._isMounted){
            if (
                Math.abs(nextState.motion.x - this.state.motion.x) < this.props.motionTolerance
                && Math.abs(nextState.motion.y - this.state.motion.y) < this.props.motionTolerance
                && Math.abs(nextState.motion.z - this.state.motion.z) < this.props.motionTolerance
            ) {
                this.detectFaces(true);
                this.detectMotion(false);
            } else {
                //moving
            }
        }

    }

    detectMotion = (doDetect) => {
        this.setState({
            detectMotion: doDetect,
        });

        if (doDetect) {
            DangerZone.DeviceMotion.setUpdateInterval(this.props.motionInterval);
        } else if (!doDetect && this.state.faceDetecting) {
            this.motionListener.remove();
        }

    }

    onDeviceMotion = (rotation) => {
        this.setState({
            motion: rotation.accelerationIncludingGravity
        });
    }


    detectFaces(doDetect) {
        this.setState({
            faceDetecting: doDetect,
        });
    }


    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (

                <View
                    style={{ flex: 1 }}
                >
                    <Camera
                        style={{ flex: 1, opacity: 0 }}
                        type={this.props.cameraType}
                        onFacesDetected={this.state.faceDetecting ? this.handleFacesDetected : undefined}
                        onFaceDetectionError={this.handleFaceDetectionError}
                        faceDetectorSettings={{
                            mode: FaceDetector.Constants.Mode.fast,
                            detectLandmarks: FaceDetector.Constants.Mode.none,
                            runClassifications: FaceDetector.Constants.Mode.none,
                        }}
                        ref={ref => {
                            this.camera = ref;
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                                position: 'absolute',
                                bottom: 0,
                            }}>
                            <Text
                                style={styles.textStandard}>
                                {this.state.faceDetected ? 'Face Detected' : 'No Face Detected'}
                            </Text>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                                width: '100%',
                                height: '100%',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                                position: 'absolute',
                                bottom: 0,
                                right: 10,
                            }}>
                        </View>

                    </Camera>
                    {
                        this.state.cameraLoaded ?
                        <Text style={StyleSheet.flatten([styles.badge, { backgroundColor: !this.state.faceDetected ? 'red' : '#12D47E' }])}>
                            {this.state.message}
                        </Text> : null
                    }
                </View>
            );
        }
    }

    handleFaceDetectionError = () => {
        //
    }
    handleFacesDetected = ({ faces }) => {

        if (faces.length >= 1) {
            this.props.getDetectedFace(true);
            this.setState({
                cameraLoaded: true,
                faceDetected: true,
            });
            if (!this.state.faceDetected && !this.state.countDownStarted) {
                this.initCountDown();
            }
            this.state.message = "Face detected";
        } else {
            this.props.getDetectedFace(false);
            this.setState({ faceDetected: false });
            this.cancelCountDown();
            this.state.message = "No face detected";
        }
    }
    initCountDown = () => {
        this.setState({
            countDownStarted: true,
        });
    }
    cancelCountDown = () => {
        clearInterval(this.countDownTimer);
        this.setState({
            countDownSeconds: this.props.countDownSeconds,
            countDownStarted: false,
        });
    }
    onPictureSaved = () => {
        this.detectFaces(false);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStandard: {
        fontSize: 18,
        marginBottom: 10,
        color: 'white'
    },
    badge: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        color: 'white',
        width: 150,
        paddingVertical: 5,
        paddingLeft: 6,
        paddingRight: 6,
    }
});