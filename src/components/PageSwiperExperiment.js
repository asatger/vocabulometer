import React, { Component } from 'react';
import {Button, Image, ScrollView, StyleSheet, Switch} from 'react-native';
import { View, Text } from 'native-base';
import Swiper from 'react-native-deck-swiper';
import { connect } from 'react-redux';
import { sendWordsRead } from '../actions';
import BackHandler from '../components/BackHandlerWrapper';
import SwitchParagraphRead from '../components/SwitchParagraphRead';
import axios from "axios";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: -30,
    },
    card: {
        padding: 16,
        flex: 1,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: "#E8E8E8",
        backgroundColor: "white",
        marginBottom: 35
    },
    text: {
        paddingVertical: 8,
        textAlign: 'left',
        fontSize: 15,
        backgroundColor: "transparent"
    },
    textScore: {
        fontSize: 22,
        textAlign: 'center',
        backgroundColor: "transparent",
        paddingVertical: 12,
    },
    pageNumber: {
        alignSelf: 'flex-end',
    },
    centeredContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredTitle: {
        textAlign: "center",
        fontSize: 24
    },
    centeredSubTitle: {
        textAlign: "center",
        fontSize: 18
    },
    SwitchBox: {
        marginBottom: 20,
        backgroundColor: "#f1f2f6",
        padding: 10,
        borderRadius: 10
    },
    switch: {
        flex: 1,
        alignItems: "flex-end",
        marginRight: 10
    },
    switchContainer: {
        flexDirection: "row",
    },
    textBox: {
        marginTop: 5,
        marginRight: 20,
        marginLeft: 20,
        justifyContent: "center",
    }
});

class PageSwiperExperiment extends Component {
    constructor(props) {
        super(props);
        this.state.switchsValue['liked'] = true;
        this.state.switchsValue['difficult'] = true;
    }

    state = {
        cardIndex: 0,
        switchsValue: [],
        confirmation: ""
    }

    onSwiped = (cardIndex, direction) => {
        this.setState({
            cardIndex: direction === 'left' ? cardIndex + 1 : cardIndex - 1
        })
    };

    toggleSwitch = (switchID, switchValue) => {
        console.log(switchID + ' ' + switchValue);
        this.state.switchsValue[switchID] = switchValue;
        return switchValue;
    }

    sendData = () => {
        let t = this;

        let url = "https://vocabulometer.iutlppro.devops.u-bordeaux.fr/api/v1/";
        //let url = "http://localhost:8080/api/v1/";

        axios.post(url + 'experiments/data', {
            id_experiment: this.props.experimentid,
            id_text: this.props.textid,
            algo_difficulty_prediction: 1,
            user_difficulty: this.state.switchsValue['liked'],
            algo_interest_prediction: 1,
            user_interest: this.state.switchsValue['difficult']
        })
            .then(function (response) {
                t.props.navigation.navigate('Main')
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    render() {
        const finalData = [];

        let t = this.props.data.split(" ");
        let tmp = "";

        for(let i = 0; i < t.length; i++) {
            tmp += t[i] + " ";

            if(i%120 === 0 && i !== 0) {
                finalData.push(tmp);
                tmp = "";
            }
        }

        finalData.push("");

        return (
            <View style={styles.container}>
                <BackHandler />
                {
                    <Swiper
                        goBackToPreviousCardOnSwipeRight
                        disableRightSwipe={this.state.cardIndex === 0}
                        cards={finalData}
                        onSwipedLeft={(cardIndex) => this.onSwiped(cardIndex, 'left')}
                        onSwipedRight={(cardIndex) => this.onSwiped(cardIndex, 'right')}
                        showSecondCard={false}
                        cardVerticalMargin={50}
                        renderCard={(card, index) => {
                            return (
                                <ScrollView contentContainerStyle={styles.card}>
                                    {
                                        this.state.cardIndex+1 === finalData.length ?
                                            <View>
                                                <View style={styles.SwitchBox}>
                                                    <View style={styles.switchContainer}>
                                                        <Text style={styles.textBox}>
                                                            Did you like the text ?
                                                        </Text>
                                                        <View style={styles.switch}>
                                                            <SwitchParagraphRead
                                                                toggleSwitch={this.toggleSwitch}
                                                                switchID={'liked'}>
                                                            </SwitchParagraphRead>
                                                        </View>
                                                    </View>
                                                </View>

                                                <View style={styles.SwitchBox}>
                                                    <View style={styles.switchContainer}>
                                                        <Text style={styles.textBox}>
                                                            The text was difficult ?
                                                        </Text>
                                                        <View style={styles.switch}>
                                                            <SwitchParagraphRead
                                                                toggleSwitch={this.toggleSwitch}
                                                                switchID={'difficult'}>
                                                            </SwitchParagraphRead>
                                                        </View>
                                                    </View>
                                                </View>

                                                <Button
                                                    full
                                                    success
                                                    onPress={this.sendData}
                                                    title="Submit"
                                                />

                                            </View>
                                            :

                                                <Text style={styles.text}>{card}</Text>

                                    }
                                    {
                                        index < finalData.length
                                            ? <Text style={styles.pageNumber}>
                                                {`Page ${index + 1} / ${finalData.length}`}
                                            </Text>
                                            : null
                                    }
                                </ScrollView>
                            )
                        }}
                        onSwipedAll={() => {
                            console.log(this.state.switchsValue);
                            this.props.finish(this.state.switchsValue);
                        }}
                        backgroundColor={'transparent'}
                        stackSize={finalData.length}>

                    </Swiper>
                }
            </View>
        );
    }
}

const mapStateToProps = ({ user }) => {
    const { token } = user;
    return { token };
};

export default connect(mapStateToProps, { sendWordsRead })(PageSwiperExperiment);
