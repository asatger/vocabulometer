import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { Button, Icon } from 'native-base';
import commonColor from '../../native-base-theme/variables/commonColor';
import { withNavigation } from 'react-navigation';

const BackButton = (props) => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <Button
                iconRight={Platform.OS === 'ios'}
                transparent
                onPress={() => props.navigation.goBack()}
            >
                <Icon type="Ionicons" name='arrow-round-back' style={{ fontSize: 30, color: "#124041" }} />
            </Button>
        </View>
    )
};

export default withNavigation(BackButton);