import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation';
import HomeScreen from '../screens/HomeScreen'
import EasyTextsScreen from '../screens/EasyTextsScreen';
import HardTextsScreen from '../screens/HardTextsScreen';
import RecommendedTextsScreen from '../screens/RecommendedTextsScreen';
import LandingScreen from '../screens/LandingScreen';
import LoginScreen from "../screens/LoginScreen";
import TextScreen from "../screens/TextScreen";
import RightNav from './RightNav';
import ContactScreen from "../screens/ContactScreen";
import VideoScreen from "../screens/VideoScreen";
import StatsScreen from "../screens/StatsScreen";
import ChooseLevelScreen from "../screens/ChooseLevelScreen";
import VideosScreen from "../screens/VideosScreen";
import ExperimentFormScreen from "../screens/ExperimentFormScreen";
import Logo from './Logo';
import BackButton from './BackButton';
import TextScreenExperiment from "../screens/TextScreenExperiment";

const HomeStackNavigator = createStackNavigator(
    {
        Main: {
            screen: HomeScreen,
            navigationOptions: {
                headerLeft: null
            }
        },

        EasyTexts: {
            screen: EasyTextsScreen,
        },

        HardTexts: {
            screen: HardTextsScreen,
        },

        DemoText: {
            screen: TextScreen,
        },

        RecommendedTexts: {
            screen: RecommendedTextsScreen,
        },

        Text: {
            screen: TextScreen,
        },

        TextExperiment: {
            screen: TextScreenExperiment
        },

        Video: {
            screen: VideoScreen,
        },

        Landing: {
            screen: LandingScreen,
        },

        Login: {
            screen: LoginScreen,
            navigationOptions: {
                header: null
            }
        },

        Stats: {
            screen: StatsScreen,
        },

        Contact: {
            screen: ContactScreen,
        },

        ChooseLevel: {
            screen: ChooseLevelScreen
        },

        Videos: {
            screen: VideosScreen
        },

        ExperimentForm: {
            screen: ExperimentFormScreen
        },
    },

    //GLOBAL CONFIG FOR ALL SCREENS
    {
        initialRouteName: 'Landing',
        navigationOptions: {
            headerRight: <RightNav/>,
            headerTitle: <Logo/>, // Logo instead of header title
            headerLeft: <BackButton/>,
            headerStyle: {
                backgroundColor: '#6DA57F',
            },
        }
    },
);
export default HomeStackNavigator;
