import React, {Component} from 'react';
import {connect} from "react-redux";
import {View,Platform} from 'react-native';
import {Button, Icon} from 'native-base';
import {logout} from "../actions";
import {withNavigation} from 'react-navigation';
class RightNav extends Component {

    render() {
        if (this.props.token) {
            return (
                <View style={{flexDirection: 'row'}}>
                    <Button
                        iconRight={Platform.OS === 'ios'} // RightIcon renders better on ios because no underlay onPress color on button
                        transparent
                        onPress={() => this.props.navigation.navigate('Contact')}
                    >
                        <Icon type="Ionicons" name='information-circle' style={{fontSize: 30, color: "#124041"}}/>
                    </Button>
                    <Button
                        iconRight={Platform.OS === 'ios'}
                        transparent
                        onPress={() => this.props.navigation.navigate('Stats')}
                    >
                        <Icon type="Ionicons" name='contact' style={{fontSize: 30, color: "#124041"}}/>
                    </Button>
                    <Button
                        iconRight={Platform.OS === 'ios'}
                        transparent
                        onPress={() => this.props.logout(this.props.token)}
                    >
                        <Icon type="Ionicons" name='log-out' style={{fontSize: 30, color: "#124041"}}/>
                    </Button>
                </View>
            );
        }
        return null;
    }
}

const mapStateToProps = ({user}) => {
    const {token} = user;
    return {token};
};

const mapDispatchToProps = {
    logout
};

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(RightNav));