# Vocabulometer app REACT NATIVE

This application uses the expo framework (Expo is a set of tools, libraries and services which let you build native iOS and Android apps by writing JavaScript, precisely React-Native)  : https://docs.expo.io/versions/v28.0.0/#introduction

# **Steps to configure the environment and launch the project :**

The expo procedure:
https://docs.expo.io/versions/v28.0.0/introduction/installation


## 1. Install Expo XDE **OR** CLI expo

Expo XDE is a graphical interface to manage, launch on emulator, and publish expo projects  https://expo.io/tools#xde

The CLI Expo provides the same functionalities on the command line but requires root access because this package must be installed globally. To install it :

    npm install -g expo-cli

## 2. Clone the repository and install dependencies

    git clone https://github.com/asatger/vocabulometer.git

    cd vocabulometer 
	
	npm i


## 3. Run the projet with CLI expo

Open a terminal and type this command :

	expo start

You will get this display :

![expo start](./documentation_assets/expo_start.PNG)

if the page has not already opened, open a web page with the url highlighted.

You should have a configuration panel like this one :

![expo web panel](./documentation_assets/expo_web_panel.PNG)

### **If you want to use an emulator :**

![tunnel connection](./documentation_assets/emulator.PNG)

then follow the instructions displayed

### **Else:**

Install the expo application on your phone

**Then**

- If your phone is connected to the same network as your computer, flash the QRCode displayed on the screen with your phone.
- If this is not the case, put the connection in tunel mode and flash the new QRCode.

![tunnel connection](./documentation_assets/connection_tunnel.PNG)


### **The application should now launch**

### 4. Run the projet with XDE expo

Open expo XDE, open existing project, choose the cloned project earlier, then click on device > open on android/ios


#### If necessary, here is the link to the [old documentation](https://gitlab.com/asatger/vocabulometer/blob/master/README_old.md) (in french)